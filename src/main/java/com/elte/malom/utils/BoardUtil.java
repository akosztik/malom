package com.elte.malom.utils;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BoardUtil {

    public String initEmptyBoard(Integer capacity) {
        List<Integer> initialBoard = new ArrayList<>(capacity);
        for (int i = 0; i < capacity; i++) {
            initialBoard.add(0);
        }
        return boardArrayToString(initialBoard);
    }

    public String boardArrayToString(List<Integer> boardArray){
        return boardArray.toString();
    }

    public List<Integer> boardStringToArray(String boardString) {
        List<String> listString = Arrays.asList(boardString.replace("[", "").replace("]", "").replace(" ", "").split(","));
        List<Integer> retval;
        try {
            retval = listString.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
        } catch (Exception e) {
            retval = new ArrayList<>();
        }
        return retval;
    }
}
