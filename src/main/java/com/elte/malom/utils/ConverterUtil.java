package com.elte.malom.utils;

import com.elte.malom.domain.Move;
import com.elte.malom.dto.BoardDto;
import com.elte.malom.dto.GameDto;
import com.elte.malom.dto.MoveDto;
import com.elte.malom.dto.PlayerDto;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.entities.Player;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;

import java.util.Date;

@Slf4j
@Configuration
public class ConverterUtil {

    private BoardUtil boardUtil;

    @Autowired
    private DelegatingPasswordEncoder delegatingPasswordEncoder;


    public ConverterUtil(BoardUtil boardUtil) {
        this.boardUtil = boardUtil;
    }

    @Bean
    public ConfigurableConversionService configurableConversionService() {
        final ConfigurableConversionService conversionService = new GenericConversionService();

        conversionService.addConverter(new BoardToBoardDtoConverter());
        conversionService.addConverter(new CopyBoardToNewBoard());
        conversionService.addConverter(new PlayerDtoToPlayerConverter());
        conversionService.addConverter(new PlayerToPlayerDtoConverter());
        conversionService.addConverter(new GameToGameDtoConverter());
        conversionService.addConverter(new GameDtoToGameConverter());
        conversionService.addConverter(new MoveDtoToMoveConverter());

        return conversionService;
    }

    public class BoardToBoardDtoConverter implements Converter<Board, BoardDto> {

        @Override
        public BoardDto convert(Board source) {
            return BoardDto.builder()
                    .board(boardUtil.boardStringToArray(source.getBoard()))
                    .gameModePlayer1(source.getGameModePlayer1())
                    .gameModePlayer2(source.getGameModePlayer2())
                    .pieceCountPlayer1(source.getPieceCountPlayer1())
                    .nextPlayer(source.getNextPlayer())
                    .nextMoveType(source.getNextMoveType())
                    .pieceCountPlayer2(source.getPieceCountPlayer2())
                    .build();
        }
    }

    public class CopyBoardToNewBoard implements Converter<Board, Board> {

        @Override
        public Board convert(Board source) {
            Board board = new Board();
            board.setGame(source.getGame());
            board.setBoard(source.getBoard());
            board.setGameModePlayer1(source.getGameModePlayer1());
            board.setGameModePlayer2(source.getGameModePlayer2());
            board.setPieceCountPlayer1(source.getPieceCountPlayer1());
            board.setNextPlayer(source.getNextPlayer());
            board.setNextMoveType(source.getNextMoveType());
            board.setPieceCountPlayer2(source.getPieceCountPlayer2());
            board.setCreated(new Date());
            return board;
        }
    }


    public class PlayerDtoToPlayerConverter implements Converter<PlayerDto, Player> {

        @Override
        public Player convert(PlayerDto source) {
            Player player = new Player();
            player.setUserName(source.getUserName());
            player.setPasswordHash(delegatingPasswordEncoder.encode(source.getPassword()));
            player.setEmail(source.getEmail());
            return player;
        }
    }

    public class PlayerToPlayerDtoConverter implements Converter<Player, PlayerDto> {

        @Override
        public PlayerDto convert(Player source) {
            PlayerDto playerDto = new PlayerDto();
            playerDto.setUserName(source.getUserName());
            playerDto.setPassword(source.getPasswordHash());
            playerDto.setEmail(source.getEmail());
            return playerDto;
        }
    }

    public class GameToGameDtoConverter implements Converter<Game, GameDto> {

        @Override
        public GameDto convert(Game source) {
            GameDto gameDto = new GameDto();
            gameDto.setId(source.getId());
            gameDto.setName(source.getName());
            gameDto.setGameStatus(source.getGameStatus());
            gameDto.setGameType(source.getGameType());
            gameDto.setDifficulty(source.getDifficulty());
            return gameDto;
        }
    }


    private class GameDtoToGameConverter implements Converter<GameDto, Game> {

        @Override
        public Game convert(GameDto source) {
            Game game = new Game();
            game.setId(source.getId());
            game.setName(source.getName());
            game.setGameStatus(source.getGameStatus());
            game.setGameType(source.getGameType());
            game.setDifficulty(source.getDifficulty());
            return game;
        }
    }

    private class MoveDtoToMoveConverter implements Converter<MoveDto, Move> {

        @Override
        public Move convert(MoveDto source) {
            Move move = new Move();
            move.setFrom(source.getFrom());
            move.setMoveType(source.getMoveType());
            move.setTo(source.getTo());
            return move;
        }
    }
}
