package com.elte.malom.utils.constans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ClassicGameBoard {

    //singleton designpattern
    private static final List<Map<Integer,Integer>> NEIGHBOURS = new ArrayList<Map<Integer, Integer>>();

    private static final ClassicGameBoard CLASSIC_GAME_BOARD = new ClassicGameBoard();

    private ClassicGameBoard(){
        Map point0 = new HashMap<Integer,Integer>();
        point0.put(1,2);
        point0.put(9,2);
        NEIGHBOURS.add(point0);

        Map point1 = new HashMap<Integer,Integer>();
        point1.put(0,2);
        point1.put(2,1);
        point1.put(4,3);
        NEIGHBOURS.add(point1);

        Map point2 = new HashMap<Integer,Integer>();
        point2.put(1,1);
        point2.put(14,1);
        NEIGHBOURS.add(point2);

        Map point3 = new HashMap<Integer,Integer>();
        point3.put(4,2);
        point3.put(10,2);
        NEIGHBOURS.add(point3);

        Map point4 = new HashMap<Integer,Integer>();
        point4.put(1,3);
        point4.put(3,2);
        point4.put(5,1);
        point4.put(7,3);
        NEIGHBOURS.add(point4);

        Map point5 = new HashMap<Integer,Integer>();
        point5.put(4,1);
        point5.put(13,1);
        NEIGHBOURS.add(point5);

        Map point6 = new HashMap<Integer,Integer>();
        point6.put(7,2);
        point6.put(11,2);
        NEIGHBOURS.add(point6);

        Map point7 = new HashMap<Integer,Integer>();
        point7.put(4,3);
        point7.put(6,2);
        point7.put(8,1);
        NEIGHBOURS.add(point7);

        Map point8 = new HashMap<Integer,Integer>();
        point8.put(7,1);
        point8.put(12,1);
        NEIGHBOURS.add(point8);

        Map point9 = new HashMap<Integer,Integer>();
        point9.put(0,2);
        point9.put(10,3);
        point9.put(21,1);
        NEIGHBOURS.add(point9);

        Map point10 = new HashMap<Integer,Integer>();
        point10.put(3,2);
        point10.put(9,3);
        point10.put(11,3);
        point10.put(18,1);
        NEIGHBOURS.add(point10);

        Map point11 = new HashMap<Integer,Integer>();
        point11.put(6,2);
        point11.put(10,3);
        point11.put(15,1);
        NEIGHBOURS.add(point11);

        Map point12 = new HashMap<Integer,Integer>();
        point12.put(8,1);
        point12.put(17,2);
        point12.put(13,3);
        NEIGHBOURS.add(point12);

        Map point13 = new HashMap<Integer,Integer>();
        point13.put(5,1);
        point13.put(12,3);
        point13.put(14,3);
        point13.put(20,2);
        NEIGHBOURS.add(point13);

        Map point14 = new HashMap<Integer,Integer>();
        point14.put(2,1);
        point14.put(13,3);
        point14.put(23,2);
        NEIGHBOURS.add(point14);

        Map point15 = new HashMap<Integer,Integer>();
        point15.put(11,1);
        point15.put(16,1);
        NEIGHBOURS.add(point15);

        Map point16 = new HashMap<Integer,Integer>();
        point16.put(15,1);
        point16.put(19,3);
        point16.put(17,2);
        NEIGHBOURS.add(point16);

        Map point17 = new HashMap<Integer,Integer>();
        point17.put(12,2);
        point17.put(16,2);
        NEIGHBOURS.add(point17);

        Map point18 = new HashMap<Integer,Integer>();
        point18.put(10,1);
        point18.put(19,1);
        NEIGHBOURS.add(point18);

        Map point19 = new HashMap<Integer,Integer>();
        point19.put(16,3);
        point19.put(18,1);
        point19.put(20,2);
        point19.put(22,3);
        NEIGHBOURS.add(point19);

        Map point20 = new HashMap<Integer,Integer>();
        point20.put(13,2);
        point20.put(19,2);
        NEIGHBOURS.add(point20);

        Map point21 = new HashMap<Integer,Integer>();
        point21.put(9,1);
        point21.put(22,1);
        NEIGHBOURS.add(point21);

        Map point22 = new HashMap<Integer,Integer>();
        point22.put(19,3);
        point22.put(21,1);
        point22.put(23,2);
        NEIGHBOURS.add(point22);

        Map point23 = new HashMap<Integer,Integer>();
        point23.put(14,2);
        point23.put(22,2);
        NEIGHBOURS.add(point23);
    }

    public static List<Map<Integer,Integer>> getClassicGameBoard() {
        return NEIGHBOURS;
    }

}