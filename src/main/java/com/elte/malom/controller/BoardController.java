package com.elte.malom.controller;

import com.elte.malom.domain.Move;
import com.elte.malom.dto.BoardDto;
import com.elte.malom.dto.MoveDto;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.enums.GameDifficulty;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import com.elte.malom.service.AiService;
import com.elte.malom.service.DummyAiService;
import com.elte.malom.service.BoardService;
import com.elte.malom.service.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/board")
public class BoardController {

    private BoardService boardService;
    private GameService gameService;
    private DummyAiService dummyAiService;
    private AiService aiService;
    private HttpSession httpSession;
    private ConfigurableConversionService configurableConversionService;

    @Autowired
    public BoardController(BoardService boardService, GameService gameService, DummyAiService dummyAiService, AiService aiService, HttpSession httpSession, ConfigurableConversionService configurableConversionService) {
        this.boardService = boardService;
        this.gameService = gameService;
        this.aiService = aiService;
        this.dummyAiService = dummyAiService;
        this.httpSession = httpSession;
        this.configurableConversionService = configurableConversionService;
    }

    @GetMapping("/latest")
    public BoardDto getLatestStateForGame() {
        Long gameId = (Long) httpSession.getAttribute("gameId");
        Board board = boardService.getLatestStateForGame(gameService.getGameById(gameId));
        return configurableConversionService.convert(board, BoardDto.class);
    }

    @PostMapping("/move")
    public BoardDto createNewBoardStatus(@RequestBody MoveDto moveDto) {

        Long gameId = (Long) httpSession.getAttribute("gameId");
        Game game = gameService.getGameById(gameId);
        Board lastBoard = boardService.getLatestStateForGame(game);
        Move move = configurableConversionService.convert(moveDto, Move.class);

        Board newBoardStatus = boardService.createNewBoardStatus(lastBoard, move);
        log.info("PLAYER MOVE posted: " + move.getMoveType() + ", " + move.getTo());
        game = gameService.updateGameStatus(game, gameService.checkCurrentGameStatus(game, newBoardStatus)).get();

        while (game.getGameType().equals(GameType.COMPUTER) && !newBoardStatus.getNextPlayer().equals(lastBoard.getNextPlayer())
                && game.getGameStatus().equals(GameStatus.IN_PROGRESS)) {
            Move aiMove = null;
            if (game.getDifficulty().equals(GameDifficulty.EASY)) {
                aiMove = dummyAiService.createMove(newBoardStatus);
            } else {
                aiMove = aiService.createMove(newBoardStatus);
            }
            newBoardStatus = boardService.createNewBoardStatus(newBoardStatus, aiMove);
            log.info("AI MOVE made: " + aiMove.getMoveType() + ", " + aiMove.getTo());
            game = gameService.updateGameStatus(game, gameService.checkCurrentGameStatus(game, newBoardStatus)).get();
        }
        return configurableConversionService.convert(newBoardStatus, BoardDto.class);
    }

    @GetMapping("/listAll")
    public List<BoardDto> getMovesInGame() {
        Long gameId = (Long) httpSession.getAttribute("gameId");
        return boardService.getAllStates(gameService.getGameById(gameId)).stream().map(board -> configurableConversionService.convert(board, BoardDto.class)).collect(Collectors.toList());
    }

    @GetMapping("/getNeighbours")
    public List<Integer> getMovesInGame(@RequestParam Integer position) {
        return boardService.getNeighbours(position);
    }

}
