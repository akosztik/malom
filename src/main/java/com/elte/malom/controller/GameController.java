package com.elte.malom.controller;

import com.elte.malom.controller.response.Response;
import com.elte.malom.dto.GameDto;

import com.elte.malom.domain.RankDto;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.entities.Player;
import com.elte.malom.enums.ResponseStatus;
import com.elte.malom.service.BoardService;
import com.elte.malom.service.GameService;
import com.elte.malom.service.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/game")
public class GameController {

    private GameService gameService;
    private PlayerService playerService;
    private BoardService boardService;
    private ConfigurableConversionService configurableConversionService;
    private HttpSession httpSession;

    @Autowired
    public GameController(GameService gameService, PlayerService playerService, BoardService boardService, ConfigurableConversionService configurableConversionService, HttpSession httpSession) {
        this.gameService = gameService;
        this.playerService = playerService;
        this.boardService = boardService;
        this.configurableConversionService = configurableConversionService;
        this.httpSession = httpSession;
    }

    @PostMapping("/create")
    public Response<GameDto> createNewGame(@RequestBody GameDto gameDto) {
        httpSession.removeAttribute("gameId");
        Game game = configurableConversionService.convert(gameDto, Game.class);
        if(gameService.gameNameTaken(game)) {
            return new Response<>(gameDto, com.elte.malom.enums.ResponseStatus.ALREADY_EXISTS);
        }
        Game gameCreated = gameService.createNewGame(playerService.getLoggedUser(), game);
        Board board = boardService.initBoard(gameCreated);
        httpSession.setAttribute("gameId", gameCreated.getId());
        log.info("new game id: " + httpSession.getAttribute("gameId") + " stored in session");
        return new Response<>(configurableConversionService.convert(gameCreated, GameDto.class), ResponseStatus.CREATED);
    }

    @GetMapping("/listAll")
    public List<GameDto> getAllGames() {
        return gameService.getAllGames()
                .stream()
                .map(game -> configurableConversionService.convert(game, GameDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/listPlayersGames")
    public List<GameDto> getGamesByPlayer() {
        return gameService.getGamesByPlayer(playerService.getLoggedUser())
                .stream()
                .map(game -> configurableConversionService.convert(game, GameDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/listWaitingForSecondPlayer")
    public List<GameDto> getGamesWaitingForSecondPlayer() {
        return gameService.getGamesWaitingForSecondPlayer(playerService.getLoggedUser())
                .stream()
                .map(game -> configurableConversionService.convert(game, GameDto.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/ranking")
    public List<RankDto> ranking() {
        return gameService.ranking();
    }

    @GetMapping("/playersWinningNumber")
    public Integer getPlayersWinningNumber() {
        return gameService.playersWonGameCount(playerService.getLoggedUser());
    }

    @PostMapping("/join")
    public GameDto joinGame(@RequestBody GameDto gameDto) {
        httpSession.removeAttribute("gameId");
        Player loggedUser = playerService.getLoggedUser();
        Game game = gameService.getGameById(gameDto.getId());
        Game joinedGame = gameService.joinGame(loggedUser, game);
        httpSession.setAttribute("gameId", joinedGame.getId());
        return configurableConversionService.convert(joinedGame, GameDto.class);
    }

    @GetMapping("/{id}")
    public GameDto getGameById(@PathVariable Long id) {
        return configurableConversionService.convert(gameService.getGameById(id), GameDto.class);
    }
}


