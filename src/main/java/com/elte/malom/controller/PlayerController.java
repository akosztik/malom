package com.elte.malom.controller;

import com.elte.malom.controller.response.Response;
import com.elte.malom.dto.PlayerDto;
import com.elte.malom.entities.Player;
import com.elte.malom.enums.ResponseStatus;
import com.elte.malom.service.PlayerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/player")
public class PlayerController {

    private PlayerService playerService;
    private ConfigurableConversionService configurableConversionService;

    @Autowired
    public PlayerController(PlayerService playerService, ConfigurableConversionService configurableConversionService) {
        this.playerService = playerService;
        this.configurableConversionService = configurableConversionService;
    }

    @PostMapping("/create")
    public Response<PlayerDto> createAccount(@RequestBody PlayerDto newPlayerDto) {
        Player player = configurableConversionService.convert(newPlayerDto, Player.class);
        if(playerService.playerAlreadyExists(player)) {
            return new Response<>(newPlayerDto, ResponseStatus.ALREADY_EXISTS);
        }
        return new Response<>(configurableConversionService.convert(playerService.createNewPlayer(player),PlayerDto.class), ResponseStatus.CREATED);
    }

    @GetMapping("/list")
    public List<PlayerDto> getPlayers() {
        return playerService.listPlayers().stream().map(player -> configurableConversionService.convert(player,PlayerDto.class)).collect(Collectors.toList());
    }

    @GetMapping("/logged")
    public Response<PlayerDto> getLoggedPlayer() {
        return new Response<>(configurableConversionService.convert(playerService.getLoggedUser(),PlayerDto.class), ResponseStatus.CREATED);
    }

    @GetMapping("/registeredPlayers")
    public Integer registeredPlayers() {
        return playerService.getRegisteredPlayers();
    }
    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

}
