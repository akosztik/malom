package com.elte.malom.controller.response;

import com.elte.malom.enums.ResponseStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response<T> {
    private T object;
    private ResponseStatus status;

    public Response(T object, ResponseStatus status) {
        this.object = object;
        this.status = status;
    }

}