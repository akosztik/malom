package com.elte.malom.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NextPlayer {

    FIRSTPLAYER(1),
    SECONDPLAYER(2);

    private final Integer colorCode;

}
