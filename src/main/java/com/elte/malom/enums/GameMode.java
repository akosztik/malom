package com.elte.malom.enums;

public enum GameMode {
    OPENING,
    MIDGAME,
    ENDGAME
}
