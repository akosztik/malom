package com.elte.malom.enums;

public enum GameStatus {
    IN_PROGRESS,
    FIRST_PLAYER_WON,
    SECOND_PLAYER_WON,
    WAITS_FOR_PLAYER
}
