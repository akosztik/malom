package com.elte.malom.enums;

public enum ResponseStatus {

    CREATED,
    ALREADY_EXISTS

}
