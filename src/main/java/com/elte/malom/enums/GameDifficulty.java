package com.elte.malom.enums;

public enum GameDifficulty {
    EASY,
    HARD
}
