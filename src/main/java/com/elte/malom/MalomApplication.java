package com.elte.malom;

import com.elte.malom.entities.Player;
import com.elte.malom.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;

@SpringBootApplication
public class MalomApplication {

	public static void main(String[] args) {
		SpringApplication.run(MalomApplication.class, args);
	}

	@Autowired
	private DelegatingPasswordEncoder delegatingPasswordEncoder;

	@Bean
	public CommandLineRunner demo(PlayerRepository playerRepository) {
		return (args) -> {
			Player player = playerRepository.findOneByUserName("ala");
			if (player == null) {
				playerRepository.save(new Player("ala", delegatingPasswordEncoder.encode("ala"), "ala"));
				playerRepository.save(new Player("mary", delegatingPasswordEncoder.encode("mary"),"mary"));
			}
		};
	}
}