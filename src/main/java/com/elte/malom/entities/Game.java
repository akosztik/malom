package com.elte.malom.entities;

import com.elte.malom.enums.GameDifficulty;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Check(constraints = "(game_type='COMPUTER' OR game_type='COMPETITION')" +
        "AND (game_status='FIRST_PLAYER_WON' OR game_status='SECOND_PLAYER_WON' OR game_status='IN_PROGRESS' OR game_status='WAITING_FOR_PLAYER' OR game_status='TIE')" )
@NoArgsConstructor
@AllArgsConstructor
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "first_player_id", nullable = false)
    private Player firstPlayer;

    @ManyToOne
    @JoinColumn(name = "second_player_id", nullable = true)
    private Player secondPlayer;

    @Enumerated(EnumType.STRING)
    private GameType gameType;

    @Enumerated(EnumType.STRING)
    private GameDifficulty difficulty;

    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @Column(name = "created", nullable = false)
    private Date created;

}
