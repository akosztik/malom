package com.elte.malom.entities;

import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Game game;

    @Column(name = "board", nullable = false)
    private String board;

    @Enumerated(EnumType.STRING)
    @Column(name = "game_mode_player_1", nullable = true)
    private GameMode gameModePlayer1;

    @Enumerated(EnumType.STRING)
    @Column(name = "game_mode_player_2", nullable = true)
    private GameMode gameModePlayer2;

    @Column(name = "piece_count_player_1", nullable = false)
    private Integer pieceCountPlayer1;

    @Column(name = "piece_count_player_2", nullable = false)
    private Integer pieceCountPlayer2;

    @Enumerated(EnumType.STRING)
    @Column(name = "next_Player", nullable = true)
    private NextPlayer nextPlayer;

    @Enumerated(EnumType.STRING)
    @Column(name = "next_move_type", nullable = true)
    private MoveType nextMoveType;

    @Column(name = "created", nullable = false)
    private Date created;

    public Board(Game game, String board, GameMode gameModePlayer1, GameMode gameModePlayer2, Integer pieceCountPlayer1, Integer pieceCountPlayer2, NextPlayer nextPlayer, MoveType moveType, Date created) {
        this.game = game;
        this.board = board;
        this.gameModePlayer1 = gameModePlayer1;
        this.gameModePlayer2 = gameModePlayer2;
        this.pieceCountPlayer1 = pieceCountPlayer1;
        this.pieceCountPlayer2 = pieceCountPlayer2;
        this.nextPlayer = nextPlayer;
        this.nextMoveType = moveType;
        this.created = created;
    }
}
