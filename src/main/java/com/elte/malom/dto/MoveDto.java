package com.elte.malom.dto;

import com.elte.malom.enums.MoveType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MoveDto {

    @NotNull
    private MoveType moveType;

    @NotNull
    private Integer to;
    private Integer from;
}
