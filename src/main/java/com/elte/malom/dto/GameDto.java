package com.elte.malom.dto;

import com.elte.malom.enums.GameDifficulty;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GameDto {

    private Long id;
    private String name;

    @NonNull
    private GameType gameType;
    private GameStatus gameStatus;
    private GameDifficulty difficulty;

}
