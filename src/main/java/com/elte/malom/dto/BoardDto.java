package com.elte.malom.dto;

import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import lombok.*;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BoardDto {

    private List<Integer> board;
    private GameMode gameModePlayer1;
    private GameMode gameModePlayer2;
    private Integer pieceCountPlayer1;
    private Integer pieceCountPlayer2;
    private NextPlayer nextPlayer;
    private MoveType nextMoveType;

}
