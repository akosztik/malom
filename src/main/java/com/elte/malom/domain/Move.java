package com.elte.malom.domain;

import com.elte.malom.enums.MoveType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Move {

    @NotNull
    private MoveType moveType;

    @NotNull
    @Min(0)
    @Max(23)
    private Integer to;

    @Min(0)
    @Max(23)
    private Integer from;
}