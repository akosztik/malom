package com.elte.malom.repository;

import com.elte.malom.entities.Game;
import com.elte.malom.entities.Player;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, Long> {

    List<Game> findAllByGameStatus(GameStatus gameStatus);

    List<Game> findByGameTypeAndGameStatus(GameType gametype, GameStatus gameStatus);

    List<Game> findAll();

    List<Game> findByFirstPlayerOrSecondPlayer(Player firstPlayer, Player secondPlayer);

    Game findOneByName(String name);
}

