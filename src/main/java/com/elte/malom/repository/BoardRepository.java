package com.elte.malom.repository;

import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoardRepository extends CrudRepository<Board, Long> {

    List<Board> findByGame(Game game);

    List<Board> findByGameOrderByIdDesc(Game game);

}