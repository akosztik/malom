package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.utils.BoardUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.elte.malom.enums.MoveType.HIT;
import static com.elte.malom.enums.MoveType.MOVE;
import static com.elte.malom.enums.NextPlayer.FIRSTPLAYER;
import static com.elte.malom.enums.NextPlayer.SECONDPLAYER;
import static com.elte.malom.utils.constans.ClassicGameBoard.getClassicGameBoard;

@Service
public class AiService {

    public static final Integer FIRSTPLAYER_COLOR_CODE = FIRSTPLAYER.getColorCode();
    public static final Integer SECONDPLAYER_COLOR_CODE = SECONDPLAYER.getColorCode();
    private static List<Map<Integer, Integer>> classicGameBoard = getClassicGameBoard();

    private BoardUtil boardUtil;

    @Autowired
    public AiService(BoardUtil boardUtil) {
        this.boardUtil = boardUtil;
    }

    public Move createMove(Board latestBoard) {
        Move aiMove = new Move();
        List<Integer> boardList = boardUtil.boardStringToArray(latestBoard.getBoard());

        if (latestBoard.getNextMoveType().equals(MOVE)) {
            aiMove.setMoveType(MOVE);
            switch (latestBoard.getGameModePlayer2()) {
                case OPENING:
                    Integer newMillOpeningGame = featureMillPosition(boardList, latestBoard, 2);
                    Integer newBlockMillOpeningGame = featureMillPosition(boardList, latestBoard, 1);
                    return getMove(aiMove, boardList, newMillOpeningGame, newBlockMillOpeningGame);

//**********************************************************************************************************//
                case MIDGAME:
                    List<Integer> newMillMidGame = getAllFeatureMillPosition(boardList, latestBoard, 2);
                    List<Integer> newBlockMillMidGame = getAllFeatureMillPosition(boardList, latestBoard, 1);
                    //aiMove = getMove(aiMove, boardList, newMillMidGame, newBlockMillMidGame);

                    for (int i = 0; i < boardList.size(); i++) {
                        if (boardList.get(i).equals(SECONDPLAYER_COLOR_CODE)) {

                            for (Integer neighbour : classicGameBoard.get(i).keySet()) {
                                if (boardList.get(neighbour).equals(0)) {
                                    aiMove.setFrom(i);
                                    aiMove.setTo(neighbour);
                                    return aiMove;
                                }
                            }
                        }
                    }
                    return aiMove;
//**********************************************************************************************************//
                case ENDGAME:
                    for (int i = 0; i < boardList.size(); i++) {
                        if (boardList.get(i).equals(SECONDPLAYER_COLOR_CODE) && aiMove.getFrom() == null) {
                            aiMove.setFrom(i);
                        } else if (boardList.get(i).equals(0) && aiMove.getFrom() != null) {
                            Integer newMillEndGame = featureMillPosition(boardList, latestBoard, 2);
                            Integer newBlockMillEndGame = featureMillPosition(boardList, latestBoard, 1);
                            return getMove(aiMove, boardList, newMillEndGame, newBlockMillEndGame);
                        }
                    }
                    return aiMove;
                default:
                    return aiMove;
            }
        } else {
            aiMove.setMoveType(HIT);
            for (int i = 0; i < boardList.size(); i++) {
                if (boardList.get(i).equals(FIRSTPLAYER_COLOR_CODE)) {
                    aiMove.setTo(i);
                    return aiMove;
                }
            }
        }
        return aiMove;
    }

    private Move getMove(Move aiMove, List<Integer> boardList, Integer newMillOpeningGame, Integer newBlockMillOpeningGame) {
        if (newMillOpeningGame != -1) {
            aiMove.setTo(newMillOpeningGame);
            return aiMove;
        } else if (newBlockMillOpeningGame != -1) {
            aiMove.setTo(newBlockMillOpeningGame);
            return aiMove;
        } else {
            aiMove.setTo(nextEmptyPlace(boardList));
            return aiMove;
        }
    }

    private ArrayList<Integer> getAllFeatureMillPosition(List<Integer> boardList, Board latestBoard, Integer color) {
        ArrayList<Integer> retval = new ArrayList<Integer>();
        Move testingMove = new Move();
        for (int i = 0; i < boardList.size(); i++) {
            if (boardList.get(i) == 0) {
                testingMove.setTo(i);
                if (isNewMillCreated(latestBoard, testingMove, color)) {
                    retval.add(i);
                }
            }
        }
        return retval;
    }

    private Integer featureMillPosition(List<Integer> boardList, Board latestBoard, Integer color) {
        Move testingMove = new Move();
        for (int i = 0; i < boardList.size(); i++) {
            if (boardList.get(i) == 0) {
                testingMove.setTo(i);
                if (isNewMillCreated(latestBoard, testingMove, color)) {
                    return i;
                }
            }
        }
        return -1;
    }

    private Integer nextEmptyPlace(List<Integer> boardList) {
        for (int i = 0; i < boardList.size(); i++) {
            if (boardList.get(i).equals(0)) {
                return i;
            }
        }
        return -1;
    }


    public boolean isNewMillCreated(Board newBoard, Move move, Integer color) {
        List<Integer> board = boardUtil.boardStringToArray(newBoard.getBoard());
        Integer to = move.getTo();
        Map<Integer, Integer> actualPointNeighbours = classicGameBoard.get(to);

        List<Integer> millPointsOneDeep = getNeighbours(newBoard, board, actualPointNeighbours, color);

        if (!millPointsOneDeep.isEmpty()) {
            if (millExistsBetweenFirstNeighbours(actualPointNeighbours, millPointsOneDeep)) {
                return true;
            } else if (actualPointNeighbours.size() == 3) {
                for (Integer point : millPointsOneDeep) {
                    if (actualPointNeighbours.get(point) == 3 && millExistsBetweenSecondNeighbours(board, classicGameBoard, point, to, true, color)) {
                        return true;
                    }
                }
            } else if (actualPointNeighbours.size() == 2) {
                for (Integer point : millPointsOneDeep) {
                    if (millExistsBetweenSecondNeighbours(board, classicGameBoard, point, to, false, color)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean millExistsBetweenSecondNeighbours(List<Integer> board, List<Map<Integer, Integer>> classicGameBoard, Integer point, Integer to, Boolean isNot, Integer color) {
        Map<Integer, Integer> secondPointNeighbours = classicGameBoard.get(point);
        for (Map.Entry<Integer, Integer> entry : secondPointNeighbours.entrySet()) {
            if (isNot) {
                if (entry.getValue().equals(3) && !entry.getKey().equals(to) && board.get(entry.getKey()).equals(color)) {
                    return true;
                }
            } else {
                if (!entry.getValue().equals(3) && !entry.getKey().equals(to) && board.get(entry.getKey()).equals(color)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean millExistsBetweenFirstNeighbours(Map<Integer, Integer> actualPointNeighbours, List<Integer> millPointsOneDeep) {
        for (int i = 0; i < millPointsOneDeep.size(); i++) {
            for (int j = 0; j < millPointsOneDeep.size(); j++) {
                if ((actualPointNeighbours.get(millPointsOneDeep.get(i)) + actualPointNeighbours.get(millPointsOneDeep.get(j))) % 3 == 0 && i != j) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<Integer> getNeighbours(Board newBoard, List<Integer> board, Map<Integer, Integer> actualPointNeighbours, Integer color) {
        List<Integer> neighbours = new ArrayList<Integer>();
        for (Map.Entry<Integer, Integer> entry : actualPointNeighbours.entrySet()) {
            Integer key = entry.getKey();
            if (board.get(key).equals(color)) {
                neighbours.add(key);
            }
        }
        return neighbours;
    }
}

