package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.utils.BoardUtil;
import com.elte.malom.utils.constans.ClassicGameBoard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DummyAiService {

    private BoardUtil boardUtil;

    @Autowired
    public DummyAiService(BoardUtil boardUtil) {
        this.boardUtil = boardUtil;
    }

    public Move createMove(Board latestBoard) {
        Move aiMove = new Move();
        List<Integer> boardList = boardUtil.boardStringToArray(latestBoard.getBoard());
        if (latestBoard.getNextMoveType().equals(MoveType.MOVE)) {
            switch (latestBoard.getGameModePlayer2()) {
                case OPENING:
                    aiMove.setMoveType(MoveType.MOVE);
                    for (int i = 0; i < boardList.size(); i++) {
                        if (boardList.get(i).equals(0)) {
                            aiMove.setTo(i);
                            return aiMove;
                        }
                    }

                case MIDGAME:
                    aiMove.setMoveType(MoveType.MOVE);
                    for (int i = 0; i < boardList.size(); i++) {
                        if (boardList.get(i).equals(NextPlayer.SECONDPLAYER.getColorCode())) {
                            for (Integer neighbour : ClassicGameBoard.getClassicGameBoard().get(i).keySet()) {
                                if (boardList.get(neighbour).equals(0)) {
                                    aiMove.setFrom(i);
                                    aiMove.setTo(neighbour);
                                    return aiMove;
                                }
                            }
                        }
                    }
                case ENDGAME:
                    aiMove.setMoveType(MoveType.MOVE);
                    boolean gotFrom = false;
                    boolean gotTo = false;
                    for (int i = 0; i < boardList.size(); i++) {
                        if (boardList.get(i).equals(NextPlayer.SECONDPLAYER.getColorCode()) && !gotFrom) {
                            aiMove.setFrom(i);
                            gotFrom = true;
                        } else if (boardList.get(i).equals(0) && !gotTo) {
                            aiMove.setTo(i);
                            gotTo = true;
                        }
                    }
                    return aiMove;
            }
        } else {
            aiMove.setMoveType(MoveType.HIT);
            for (int i = 0; i < boardList.size(); i++) {
                if (boardList.get(i).equals(NextPlayer.FIRSTPLAYER.getColorCode())) {
                    aiMove.setTo(i);
                    return aiMove;
                }
            }

        }
        return aiMove;
    }
}