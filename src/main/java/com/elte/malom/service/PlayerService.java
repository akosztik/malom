package com.elte.malom.service;

import com.elte.malom.entities.Player;
import com.elte.malom.repository.PlayerRepository;
import com.elte.malom.security.ContextUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

//import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
@Service
@Transactional
public class PlayerService {

    private PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository){
        this.playerRepository = playerRepository;
    }

    public boolean playerAlreadyExists(Player player) {
        Player repoPlayer = playerRepository.findOneByUserName(player.getUserName());
        if (repoPlayer != null){
            log.info("Player:  " + player.getUserName() + " already exists.");
            return true;
        }
        log.info("Player:  " + player.getUserName() + " does not exists.");
        return false;
    }

    public Player createNewPlayer(Player player){
        log.info("Player:  " + player.getUserName() + " created.");
        return playerRepository.save(player);
    }

    public Player getLoggedUser(){
        ContextUser principal = (ContextUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return playerRepository.findOneByUserName(principal.getPlayer().getUserName());
    }

    public List<Player> listPlayers(){
        return playerRepository.findAll();
     }

    public Integer getRegisteredPlayers() {
        return playerRepository.findAll().size();
    }
}
