package com.elte.malom.service;

import com.elte.malom.domain.RankDto;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.entities.Player;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.repository.GameRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GameService {

    private GameRepository gameRepository;
    private GameLogicService gameLogicService;

    @Autowired
    public GameService(GameRepository gameRepository, GameLogicService gameLogicService) {
        this.gameRepository = gameRepository;
        this.gameLogicService = gameLogicService;
    }

    public Game createNewGame(Player player, Game game) {
        Game newGame = new Game();
        newGame.setName(game.getName());
        newGame.setFirstPlayer(player);
        newGame.setGameType(game.getGameType());
        newGame.setDifficulty(game.getDifficulty());
        newGame.setGameStatus(game.getGameType().equals(GameType.COMPETITION) ? GameStatus.WAITS_FOR_PLAYER : GameStatus.IN_PROGRESS);
        newGame.setCreated(new Date());
        log.info("Game created:" + newGame.getGameType() + ", " + newGame.getFirstPlayer());
        return gameRepository.save(newGame);
    }

    public Optional<Game> updateGameStatus(Game game, GameStatus gameStatus) {
        log.info("GameStatus updated from: " + game.getGameStatus());
        Optional<Game> gameToUpdate = gameRepository.findById(game.getId());
        gameToUpdate.get().setGameStatus(gameStatus);
        log.info("GameStatus updated to: " + gameToUpdate.get().getGameStatus());
        return Optional.of(gameRepository.save(gameToUpdate.get()));
    }

    public List<Game> getAllGames() {
        log.info("Getting all games");
        return gameRepository.findAll();
    }

    public List<Game> getGamesWaitingForSecondPlayer(Player player) {
        log.info("Getting games with type: COMPETITON and status WAITS FOR PLAYER");
        return gameRepository.findByGameTypeAndGameStatus(GameType.COMPETITION, GameStatus.WAITS_FOR_PLAYER)
                .stream().filter(game -> {
                    Player firstPlayer = game.getFirstPlayer();
                    boolean playersAreDifferent = !firstPlayer.equals(player);
                    log.info("Game:" + game.getId() + " fp:" + firstPlayer.getId() + ", playerLogged: " + player.getId() + ", boolean: " + playersAreDifferent);
                    return playersAreDifferent;
                }).collect(Collectors.toList());

    }

    public List<Game> getGamesByPlayer(Player player) {
        log.info("Getting all games of player: " + player.getId());
        return gameRepository.findByFirstPlayerOrSecondPlayer(player, player);
    }

    public Game joinGame(Player player, Game game) {
        if (!game.getFirstPlayer().equals(player)) {
            game.setSecondPlayer(player);
            game.setGameStatus(GameStatus.IN_PROGRESS);
            log.info("Player:  " + player.getUserName() + " joined game: " + game.getId());
            return gameRepository.save(game);
        }
        return game;
    }

    public Game getGameById(Long id) {
        return gameRepository.findById(id).get();
    }

    public GameStatus checkCurrentGameStatus(Game game, Board lastBoard) {

        Boolean isWinner = gameLogicService.isWinner(lastBoard);

        if (isWinner && lastBoard.getNextPlayer().equals(NextPlayer.FIRSTPLAYER)) {
            log.info("There is a winner!");
            return GameStatus.SECOND_PLAYER_WON;
        } else if (isWinner && lastBoard.getNextPlayer().equals(NextPlayer.SECONDPLAYER)) {
            return GameStatus.FIRST_PLAYER_WON;
        } else if (game.getGameType() == GameType.COMPETITION && game.getSecondPlayer() == null) {
            return GameStatus.WAITS_FOR_PLAYER;
        } else {
            return GameStatus.IN_PROGRESS;
        }
    }

    public boolean gameNameTaken(Game game) {
        Game repoGame = gameRepository.findOneByName(game.getName());
        if (repoGame != null) {
            log.info("Game:  " + game.getName() + " already taken.");
            return true;
        }
        log.info("Game:  " + game.getName() + " does not taken.");
        return false;
    }

    public List<RankDto> ranking() {
        List<Game> firstPlayerWonGames = gameRepository.findAllByGameStatus(GameStatus.FIRST_PLAYER_WON);
        List<Game> secondPlayerWonGames = gameRepository.findAllByGameStatus(GameStatus.SECOND_PLAYER_WON);

        List<RankDto> ranks = new ArrayList<RankDto>();
        Map<String, Integer> rank = new HashMap<>();

        countWonGames(1, firstPlayerWonGames, rank);
        countWonGames(2, secondPlayerWonGames, rank);

        Map<String, Integer> sortedRanks = sortByValue(rank);
        Integer order = 1;
        for (String name : sortedRanks.keySet()) {
            ranks.add(new RankDto(order, name, sortedRanks.get(name)));
            order++;
        }
        return ranks;
    }

    public static Map<String, Integer> sortByValue(final Map<String, Integer> wordCounts) {
        return wordCounts.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    private void countWonGames(Integer wonedPlayer, List<Game> playerList, Map<String, Integer> rank) {
        String name = "";
        for (Game game : playerList) {
            if (wonedPlayer == 1) {
                name = game.getFirstPlayer().getUserName();
            } else {
                name = game.getSecondPlayer().getUserName();
            }
            if (!rank.containsKey(name)) {
                rank.put(name, 1);
            } else {
                rank.put(name, rank.get(name) + 1);

            }
        }
    }

    public Integer playersWonGameCount(Player loggedUser) {

        List<Game> firstPlayerWonGames = gameRepository.findAllByGameStatus(GameStatus.FIRST_PLAYER_WON);
        List<Game> secondPlayerWonGames = gameRepository.findAllByGameStatus(GameStatus.SECOND_PLAYER_WON);
        return getWonGameCountForSpecPlayer(1, loggedUser, firstPlayerWonGames)
                + getWonGameCountForSpecPlayer(2, loggedUser, secondPlayerWonGames);
    }

    private Integer getWonGameCountForSpecPlayer(Integer wonedPlayer, Player loggedUser, List<Game> firstPlayerWonGames) {
        Integer winningCount = 0;
        for (Game game : firstPlayerWonGames) {
            if (wonedPlayer == 1 && game.getFirstPlayer().getId().equals(loggedUser.getId())) {
                winningCount += 1;
            } else if (wonedPlayer == 2 && game.getSecondPlayer().getId().equals(loggedUser.getId())) {
                winningCount += 1;
            }
        }
        return winningCount;
    }
}