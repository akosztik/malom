package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.utils.constans.ClassicGameBoard;
import com.elte.malom.utils.BoardUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class GameLogicService {

    private ConfigurableConversionService configurableConversionService;
    private BoardUtil boardUtil;

    @Autowired
    public GameLogicService(ConfigurableConversionService configurableConversionService, BoardUtil boardUtil) {
        this.configurableConversionService = configurableConversionService;
        this.boardUtil = boardUtil;
    }

    public Boolean isWinner(Board lastBoard) {

        Boolean isWinner = false;
        List<Integer> board = boardUtil.boardStringToArray(lastBoard.getBoard());
        Integer nextPlayerColor = lastBoard.getNextPlayer().getColorCode();

        if (checkGameMode(lastBoard).equals(GameMode.ENDGAME)) {
            isWinner = checkEndGameWinner(board, nextPlayerColor);
        } else if (checkGameMode(lastBoard).equals(GameMode.MIDGAME)) {
            isWinner = checkMidGameWinner(board, nextPlayerColor);
        }
        return isWinner;
    }

    private Boolean checkEndGameWinner(List<Integer> board, Integer nextPlayerColor) {
        Integer pieceSum = 0;
        for (Integer piece : board) {
            if (piece.equals(nextPlayerColor)) {
                pieceSum += 1;
            }
        }
        if (pieceSum <= 2) {
            return true;
        }
        return false;
    }

    private Boolean checkMidGameWinner(List<Integer> board, Integer nextPlayerColor) {
        for (int i = 0; i < board.size(); i++) {
            if (board.get(i).equals(nextPlayerColor)) {
                for (Integer neighbour : ClassicGameBoard.getClassicGameBoard().get(i).keySet()) {
                    if (board.get(neighbour).equals(0)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public GameMode checkGameMode(Board lastBoardStatus) {
        NextPlayer actualPlayer = lastBoardStatus.getNextPlayer();
        return (actualPlayer == NextPlayer.FIRSTPLAYER) ? lastBoardStatus.getGameModePlayer1() : lastBoardStatus.getGameModePlayer2();
    }

    public Board hitOnBoard(Board lastBoardStatus, Move move) {
        NextPlayer nextPlayer = lastBoardStatus.getNextPlayer();
        Board newBoard = configurableConversionService.convert(lastBoardStatus, Board.class);

        List<Integer> board = boardUtil.boardStringToArray(newBoard.getBoard());
        board.set(move.getTo(), 0);

        newBoard.setBoard(boardUtil.boardArrayToString(board));
        newBoard.setNextPlayer(nextPlayer == NextPlayer.FIRSTPLAYER ? NextPlayer.SECONDPLAYER : NextPlayer.FIRSTPLAYER);
        newBoard.setNextMoveType(MoveType.MOVE);

        Integer pieceSum = countPlayerPieces(newBoard, board);
        if (checkGameMode(newBoard).equals(GameMode.MIDGAME)  &&  pieceSum == 3) {
            if(newBoard.getNextPlayer().equals(NextPlayer.FIRSTPLAYER)){
                newBoard.setGameModePlayer1(GameMode.ENDGAME);
            }else{
                newBoard.setGameModePlayer2(GameMode.ENDGAME);
            }
        }
        return newBoard;
    }

    private Integer countPlayerPieces(Board newBoard, List<Integer> board) {
        Integer pieceSum = 0;
        for (Integer piece : board) {
            if (piece == newBoard.getNextPlayer().getColorCode()) {
                pieceSum += 1;
            }
        }
        return pieceSum;
    }

    public Board moveOnBoard(Board lastBoardStatus, Move move) {
        GameMode gameMode = checkGameMode(lastBoardStatus);
        NextPlayer nextPlayer = lastBoardStatus.getNextPlayer();

        Board newBoard = configurableConversionService.convert(lastBoardStatus, Board.class);
        List<Integer> board = boardUtil.boardStringToArray(newBoard.getBoard());

        switch (gameMode) {
            case OPENING:
                newBoard = setPieceCount(newBoard, nextPlayer);
                board.set(move.getTo(), nextPlayer.getColorCode());
                newBoard.setBoard(boardUtil.boardArrayToString(board));
                if (!isNewMillCreated(newBoard, move)) {
                    newBoard.setNextPlayer(nextPlayer == NextPlayer.FIRSTPLAYER ? NextPlayer.SECONDPLAYER : NextPlayer.FIRSTPLAYER);
                    newBoard.setNextMoveType(MoveType.MOVE);
                } else {
                    newBoard.setNextMoveType(MoveType.HIT);
                }
                break;
            //TODO: test
            default:
                board.set(move.getFrom(), 0);
                board.set(move.getTo(), nextPlayer.getColorCode());
                newBoard.setBoard(boardUtil.boardArrayToString(board));
                if (!isNewMillCreated(newBoard, move)) {
                    newBoard.setNextPlayer(nextPlayer == NextPlayer.FIRSTPLAYER ? NextPlayer.SECONDPLAYER : NextPlayer.FIRSTPLAYER);
                    newBoard.setNextMoveType(MoveType.MOVE);
                } else {
                    newBoard.setNextMoveType(MoveType.HIT);
                }
                break;
        }
        return newBoard;
    }

    private Board setPieceCount(Board newBoard, NextPlayer nextPlayer) {
        Integer pieceCount;
        if ((nextPlayer == NextPlayer.FIRSTPLAYER)) {
            pieceCount = newBoard.getPieceCountPlayer1() - 1;
            newBoard.setPieceCountPlayer1(pieceCount);
            if (pieceCount.equals(0)) {
                newBoard.setGameModePlayer1(GameMode.MIDGAME);
            }
        } else {
            pieceCount = newBoard.getPieceCountPlayer2() - 1;
            newBoard.setPieceCountPlayer2(pieceCount);
            if (pieceCount.equals(0)) {
                newBoard.setGameModePlayer2(GameMode.MIDGAME);
            }
        }
        return newBoard;
    }

    public boolean isNewMillCreated(Board newBoard, Move move) {
        List<Integer> board = boardUtil.boardStringToArray(newBoard.getBoard());
        List<Map<Integer, Integer>> classicGameBoard = ClassicGameBoard.getClassicGameBoard();
        Integer to = move.getTo();
        Map<Integer, Integer> actualPointNeighbours = classicGameBoard.get(to);

        List<Integer> millPointsOneDeep = getNeighbours(newBoard, board, actualPointNeighbours);

        if (!millPointsOneDeep.isEmpty()) {
            if (millExistsBetweenFirstNeighbours(actualPointNeighbours, millPointsOneDeep)) {
                return true;
            } else if (actualPointNeighbours.size() == 3) {
                for (Integer point : millPointsOneDeep) {
                    if (actualPointNeighbours.get(point) == 3 && millExistsBetweenSecondNeighbours(newBoard, board, classicGameBoard, point, to, true)) {
                        return true;
                    }
                }
            } else if (actualPointNeighbours.size() == 2) {
                for (Integer point : millPointsOneDeep) {
                    if (millExistsBetweenSecondNeighbours(newBoard, board, classicGameBoard, point, to, false)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean millExistsBetweenSecondNeighbours(Board newBoard, List<Integer> board, List<Map<Integer, Integer>> classicGameBoard, Integer point, Integer to, Boolean isNot) {
        Map<Integer, Integer> secondPointNeighbours = classicGameBoard.get(point);
        for (Map.Entry<Integer, Integer> entry : secondPointNeighbours.entrySet()) {
            if (isNot) {
                if (entry.getValue().equals(3) && !entry.getKey().equals(to) && board.get(entry.getKey()).equals(newBoard.getNextPlayer().getColorCode())) {
                    return true;
                }
            } else {
                if (!entry.getValue().equals(3) && !entry.getKey().equals(to) && board.get(entry.getKey()).equals(newBoard.getNextPlayer().getColorCode())) {
                    return true;
                }
            }
        }
        return false;
    }


    private boolean millExistsBetweenFirstNeighbours(Map<Integer, Integer> actualPointNeighbours, List<Integer> millPointsOneDeep) {
        for (int i = 0; i < millPointsOneDeep.size(); i++) {
            for (int j = 0; j < millPointsOneDeep.size(); j++) {
                if ((actualPointNeighbours.get(millPointsOneDeep.get(i)) + actualPointNeighbours.get(millPointsOneDeep.get(j))) % 3 == 0 && i != j) {
                    return true;
                }
            }
        }
        return false;
    }

    private List<Integer> getNeighbours(Board newBoard, List<Integer> board, Map<Integer, Integer> actualPointNeighbours) {
        List<Integer> neighbours = new ArrayList<Integer>();
        for (Map.Entry<Integer, Integer> entry : actualPointNeighbours.entrySet()) {
            Integer key = entry.getKey();
            if (board.get(key).equals(newBoard.getNextPlayer().getColorCode())) {
                neighbours.add(key);
            }
        }
        return neighbours;
    }
}
