package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.repository.BoardRepository;
import com.elte.malom.utils.BoardUtil;
import com.elte.malom.utils.constans.ClassicGameBoard;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
@Transactional
public class BoardService {

    private BoardUtil boardUtil;
    private BoardRepository boardRepository;
    private GameLogicService gameLogicService;

    @Autowired
    public BoardService(BoardRepository boardRepository, BoardUtil boardUtil, GameLogicService gameLogicService) {
        this.boardRepository = boardRepository;
        this.boardUtil = boardUtil;
        this.gameLogicService = gameLogicService;
    }

    public Board initBoard(Game game) {
        String initialBoard = boardUtil.initEmptyBoard(24);
        Board board = new Board(game, initialBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        log.info("initialized board:" + board.getBoard() + ", " + board.getNextPlayer());
        return boardRepository.save(board);
    }

    public Board getLatestStateForGame(Game game) {
        return boardRepository.findByGameOrderByIdDesc(game).get(0);
    }

    public Board createNewBoardStatus(Board lastBoard, Move move) {
        Board newBoardStatus;
        if (move.getMoveType() == MoveType.HIT) {
            newBoardStatus = gameLogicService.hitOnBoard(lastBoard, move);
        } else {
            newBoardStatus = gameLogicService.moveOnBoard(lastBoard, move);
        }
        log.info("Saving new board:" + newBoardStatus.getBoard() + ", " + newBoardStatus.getNextPlayer());
        return boardRepository.save(newBoardStatus);
    }

    public List<Board> getAllStates(Game game) {
        log.info("Getting all states of game: " + game.getId());
        return  boardRepository.findByGame(game);
    }

    public List<Integer> getNeighbours(Integer position) {
        List<Integer> neighbours = new ArrayList<Integer>();
        neighbours.addAll(ClassicGameBoard.getClassicGameBoard().get(position).keySet());
        return neighbours;
    }
}
