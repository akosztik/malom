package com.elte.malom.security;

import com.elte.malom.entities.Player;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import static com.google.common.collect.ImmutableSet.of;

public class ContextUser extends User {

    private final Player player;

    public ContextUser(Player player) {
        super(player.getUserName(),
                player.getPasswordHash(),
                true,
                true,
                true,
                true,
                of(new SimpleGrantedAuthority("create")));

        this.player = player;
    }

    public Player getPlayer() {
        return  player;
    }
}
