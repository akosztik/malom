package com.elte.malom;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TestConfiguration.class)
public class MalomApplicationTests {
    

	@Test
	public void contextLoads() {
	}

}
