package com.elte.malom.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class BoardUtilBasicTest {

    private BoardUtil boardUtil;

    @Before
    public void init() {
        boardUtil = new BoardUtil();
    }


    //basic unit test:
    @Test
    public void boardArrayToStringTest() {
        //given
        List<Integer> testBoard = Arrays.asList(1, 2, 0, 0);
        String accepted = "[1, 2, 0, 0]";

        //when
        String actual = boardUtil.boardArrayToString(testBoard);

        //then
        Assert.assertEquals(accepted, actual);
    }

}
