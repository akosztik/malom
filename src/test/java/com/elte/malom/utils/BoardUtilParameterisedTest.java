package com.elte.malom.utils;

import junitparams.JUnitParamsRunner;
import junitparams.NamedParameters;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class BoardUtilParameterisedTest {

    //Parameterised tests:
    BoardUtil boardUtil;

    @NamedParameters("testArrayToStringParameters")
    private Object[] testArrayToStringParameters() {
        return new Object[]{
                new Object[]{Arrays.asList(1, 2, 3, 4), "[1, 2, 3, 4]"},
                new Object[]{Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0), "[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]"},
                new Object[]{Arrays.asList(0), "[0]"},
                new Object[]{Arrays.asList(), "[]"}
        };
    }

    @NamedParameters("testInitEmptyBoardParameters")
    private Object[] testInitEmptyBoardParameters() {
        return new Object[]{
                new Object[]{1, "[0]"},
                new Object[]{0, "[]"},
                new Object[]{5, "[0, 0, 0, 0, 0]"},
                new Object[]{10, "[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]"}
        };
    }

    @NamedParameters("testStringToArrayParameters")
    private Object[] testStringToArrayParameters() {
        return new Object[]{
                new Object[]{"[1, 2, 3, 4]", Arrays.asList(1, 2, 3, 4)},
                new Object[]{"[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]", Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0)},
                new Object[]{"[0]", Arrays.asList(0)},
                new Object[]{"[]", Arrays.asList()}
        };
    }

    @Before
    public void init() {
        boardUtil = new BoardUtil();
    }

    @Test
    @Parameters(method = "testArrayToStringParameters")
    public void boardArrayToStringTest(List<Integer> given, String accepted) {
        String actual = boardUtil.boardArrayToString(given);
        assertEquals(accepted, actual);
    }

    @Test
    @Parameters(method = "testInitEmptyBoardParameters")
    public void initEmptyBoardTest(Integer given, String accepted) {
        String actual = boardUtil.initEmptyBoard(given);
        assertEquals(accepted, actual);
    }

    @Test
    @Parameters(method = "testStringToArrayParameters")
    public void stringToArrayTest(String given, List<Integer> accepted) {

        List<Integer> actual = boardUtil.boardStringToArray(given);
        assertEquals(actual.size(), accepted.size());

        for (int i = 0; i < accepted.size(); i++) {
            assertEquals(actual.get(i), accepted.get(i));
        }
    }

}
