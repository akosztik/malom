package com.elte.malom.service;

import com.elte.malom.entities.Player;
import com.elte.malom.repository.PlayerRepository;
import com.elte.malom.security.ContextUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)

public class PlayerServiceTest {

    @Mock
    private PlayerRepository playerRepository;

    private PlayerService playerService;

    @Before
    public void init(){
        playerService = new PlayerService(playerRepository);
    }

    @Test
    public void createNewPlayerTest(){
        //mock
        Player player = new Player((long) 12, "lola", new BCryptPasswordEncoder().encode("lola"), "lola@gmail.com");
        when(playerRepository.save(any())).thenReturn(player);

        //given
        Player playerToSave = new Player();
        playerToSave.setUserName("lola");
        playerToSave.setEmail("lola@gmail.com");
        playerToSave.setPasswordHash(new BCryptPasswordEncoder().encode("lola"));

        //when
        Player actual = playerService.createNewPlayer(playerToSave);
        Player accepted = new Player((long) 12, "lola", new BCryptPasswordEncoder().encode("lola"), "lola@gmail.com");

        assertEquals(accepted.getEmail(), actual.getEmail());
        assertEquals(accepted.getUserName(), actual.getUserName());
        assertEquals(accepted.getId(), actual.getId());
    }

    @Test
    public void getLoggedUserTest(){
        // given
        Player player = new Player((long) 12, "lola", new BCryptPasswordEncoder().encode("lola"), "lola@gmail.com");
        ContextUser principal = new ContextUser(player);
        SecurityContext securityContext = mock(SecurityContext.class);
        Authentication authentication = mock(Authentication.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        // mock
        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(principal);
        when(playerRepository.findOneByUserName("lola")).thenReturn(player);

        //when
        Player actual = playerService.getLoggedUser();
        Player accepted = new Player((long) 12, "lola", new BCryptPasswordEncoder().encode("lola"), "lola@gmail.com");

        //then
        assertEquals(accepted.getEmail(), actual.getEmail());
        assertEquals(accepted.getUserName(), actual.getUserName());
        assertEquals(accepted.getId(), actual.getId());
    }

    @Test
    public void listPlayersTest() {
        // mock - given
        List<Player> original = new ArrayList<Player>();
        original.add(new Player());
        original.add(new Player());

        when(playerRepository.findAll()).thenReturn(original);

        //when
        List<Player> actual = playerService.listPlayers();

        //then
        assertEquals(2, actual.size());
    }
}
