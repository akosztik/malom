package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.utils.BoardUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DummyAiServiceTest {
         @Mock
        private BoardUtil boardUtil;

        private DummyAiService dummyAiService;

        @Before
        public void init() {
            dummyAiService = new DummyAiService(boardUtil);
        }

        @Test
        public void createMoveOpening() {
            // mock
            Game actualGame = new Game();
            String lastBoard = "[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
            Board latestBoard = new Board(actualGame, lastBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());


            //given
            Integer[] array = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            List<Integer> list = Arrays.asList(array);
            when(boardUtil.boardStringToArray(anyString())).thenReturn(list);
            //when
            Move actual = dummyAiService.createMove(latestBoard);
            Move accepted = new Move();
            accepted.setTo(1);
            accepted.setMoveType(MoveType.MOVE);

            //then
            assertEquals(accepted.getTo(), actual.getTo());
            assertEquals(accepted.getMoveType(), actual.getMoveType());

        }
        @Test
        public void createMoveMidGame() {
            // mock
            Game actualGame = new Game();
            String lastBoard = "[1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0]";
            Board latestBoard = new Board(actualGame, lastBoard, GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

            Integer[] array = {1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0};
            List<Integer> list = Arrays.asList(array);

            Move accepted = new Move();
            accepted.setFrom(9);
            accepted.setTo(21);
            accepted.setMoveType(MoveType.MOVE);

            //given
            when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

            //when
            Move actual = dummyAiService.createMove(latestBoard);

            //then
            assertEquals(accepted.getFrom(), actual.getFrom());
            assertEquals(accepted.getTo(), actual.getTo());
            assertEquals(accepted.getMoveType(), actual.getMoveType());

        }
        @Test
        public void createMoveEndGame() {
            // mock
            Game actualGame = new Game();
            String lastBoard = "[1,1,1,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
            Board latestBoard = new Board(actualGame, lastBoard, GameMode.ENDGAME, GameMode.ENDGAME, 9, 9, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

            //given
            Integer[] array = {1, 1, 1, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            List<Integer> list = Arrays.asList(array);
            when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

            //when
            Move actual = dummyAiService.createMove(latestBoard);
            Move accepted = new Move();
            accepted.setFrom(6);
            accepted.setTo(3);
            accepted.setMoveType(MoveType.MOVE);

            //then
            Assert.assertEquals(accepted.getFrom(), actual.getFrom());
            Assert.assertEquals(accepted.getTo(), actual.getTo());
            assertEquals(accepted.getMoveType(), actual.getMoveType());

        }
        @Test
        public void createMoveHit() {
            // mock
            Game actualGame = new Game();
            String lastBoard = "[0,0,0,0,0,0,0,2,2,2,1,0,1,0,0,1,0,0,0,0,0,0,0,0]";
            Board latestBoard = new Board(actualGame, lastBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.SECONDPLAYER, MoveType.HIT, new Date());

            Integer[] array = {0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0};
            List<Integer> list = Arrays.asList(array);

            Move accepted = new Move();
            accepted.setTo(10);
            accepted.setMoveType(MoveType.HIT);

            //given
            when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

            //when
            Move actual = dummyAiService.createMove(latestBoard);

            //then
            Assert.assertEquals(accepted.getTo(), actual.getTo());
            assertEquals(accepted.getMoveType(), actual.getMoveType());

        }

}
