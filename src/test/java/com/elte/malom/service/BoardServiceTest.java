package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.GameType;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.repository.BoardRepository;
import com.elte.malom.utils.BoardUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class BoardServiceTest {

    @Mock
    private BoardUtil boardUtil;
    @Mock
    private BoardRepository boardRepository;
    @Mock
    private GameLogicService gameLogicService;

    private BoardService boardService;

    @Before
    public void init() {
        boardService = new BoardService(boardRepository, boardUtil, gameLogicService);
    }

    @Test
    public void initBoardTest() {

        // mock
        Game actualGame = new Game();
        actualGame.setGameType(GameType.COMPETITION);
        actualGame.setCreated(new Date(1109164));

        String emptyBoard = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";

        Board accepted = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        //given
        when(boardUtil.initEmptyBoard(any())).thenReturn(emptyBoard);
        when(boardRepository.save(any())).thenReturn(accepted);

        //when
        Board actual = boardService.initBoard(actualGame);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        assertEquals(accepted.getGame(), actual.getGame());
        assertEquals(accepted.getCreated(), actual.getCreated());
        assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void getAllStatesTest() {

        // mock - given
        Game actualGame = new Game();
        actualGame.setGameType(GameType.COMPETITION);
        actualGame.setCreated(new Date(1109164));
        actualGame.setId(1L);

        List<Board> accepted = new ArrayList<Board>();
        accepted.add(new Board());
        accepted.add(new Board());

        when(boardRepository.findByGame(any())).thenReturn(accepted);

        //when
        List<Board> actual = boardService.getAllStates(actualGame);

        //then
        assertEquals(accepted.size(), actual.size());
    }

    @Test
    public void getLatestStateForGameTest() {

        // mock - given
        Game actualGame = new Game();
        actualGame.setGameType(GameType.COMPETITION);
        actualGame.setCreated(new Date(1109164));
        actualGame.setId(1L);

        String emptyBoard = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
        Board accepted = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        List<Board> boardList = new ArrayList<Board>();
        boardList.add(accepted);
        boardList.add(new Board());

        when(boardRepository.findByGameOrderByIdDesc(any())).thenReturn(boardList);

        //when
        Board actual = boardService.getLatestStateForGame(actualGame);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        assertEquals(accepted.getGame(), actual.getGame());
        assertEquals(accepted.getCreated(), actual.getCreated());
        assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void hitOnBoardTest() {

        // given
        Game actualGame = new Game();
        actualGame.setGameType(GameType.COMPETITION);
        actualGame.setCreated(new Date(1109164));
        actualGame.setId(1L);

        Move move = new Move();
        move.setMoveType(MoveType.HIT);

        String emptyBoard = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
        Board boardHit = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        Board accepted = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        // mock
        when(gameLogicService.hitOnBoard(any(), any())).thenReturn(boardHit);
        when(boardRepository.save(any())).thenReturn(accepted);

        //when
        Board actual = boardService.createNewBoardStatus(boardHit, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        assertEquals(accepted.getGame(), actual.getGame());
        assertEquals(accepted.getCreated(), actual.getCreated());
        assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void moveOnBoardTest() {

        // given
        Game actualGame = new Game();
        actualGame.setGameType(GameType.COMPETITION);
        actualGame.setCreated(new Date(1109164));
        actualGame.setId(1L);

        Move move = new Move();
        move.setMoveType(MoveType.MOVE);

        String emptyBoard = "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
        Board boardMove = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        Board accepted = new Board(actualGame, emptyBoard, GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        // mock
        when(gameLogicService.moveOnBoard(any(), any())).thenReturn(boardMove);
        when(boardRepository.save(any())).thenReturn(accepted);

        //when
        Board actual = boardService.createNewBoardStatus(boardMove, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        assertEquals(accepted.getGame(), actual.getGame());
        assertEquals(accepted.getCreated(), actual.getCreated());
        assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

}
