package com.elte.malom.service;

import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.entities.Player;
import com.elte.malom.enums.GameStatus;
import com.elte.malom.enums.GameType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.repository.GameRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class GameServiceTest {

    @Mock
    private GameRepository gameRepository;

    @Mock
    private GameLogicService gameLogicService;

    private GameService gameService;

    @Before
    public void init() {
        gameService = new GameService(gameRepository, gameLogicService);
    }

    @Test
    public void createNewGameTest() {
        // mock
        Player player = new Player();

        Game originalGame = new Game();
        originalGame.setGameType(GameType.COMPETITION);
        originalGame.setCreated(new Date(1109164));

        Game mockGame = new Game();
        mockGame.setId(1L);
        mockGame.setGameType(GameType.COMPETITION);
        mockGame.setCreated(new Date(154109164));
        mockGame.setGameStatus(GameStatus.WAITS_FOR_PLAYER);

        Game accepted = new Game();
        accepted.setId(1L);
        accepted.setGameType(GameType.COMPETITION);
        accepted.setCreated(new Date(154109164));
        accepted.setGameStatus(GameStatus.WAITS_FOR_PLAYER);

        //given
        when(gameRepository.save(any())).thenReturn(mockGame);

        //when

        Game actual = gameService.createNewGame(player, originalGame);

        //then
        assertEquals(accepted.getId(), actual.getId());
        assertEquals(accepted.getGameType(), actual.getGameType());
        assertEquals(accepted.getGameStatus(), actual.getGameStatus());
        assertEquals(accepted.getCreated(), actual.getCreated());

    }

    @Test
    public void updateGameStatusTest() {
        // mock
        Game originalGame = new Game();
        originalGame.setId(1L);
        originalGame.setGameStatus(GameStatus.WAITS_FOR_PLAYER);

        Optional<Game> retval = Optional.of(originalGame);

        //given
        GameStatus acceptedGameStatus = GameStatus.FIRST_PLAYER_WON;

        Game acceptedGame = new Game();
        acceptedGame.setId(1L);
        acceptedGame.setGameStatus(acceptedGameStatus);
        Optional<Game> accepted = Optional.of(acceptedGame);

        //when
        when(gameRepository.findById(anyLong())).thenReturn(retval);

        when(gameRepository.save(retval.get())).thenReturn(acceptedGame);
        Optional<Game> actual = gameService.updateGameStatus(originalGame, acceptedGameStatus);

        //then
        assertEquals(accepted.get().getGameStatus(), actual.get().getGameStatus());
        assertEquals(accepted.get().getId(), actual.get().getId());
    }

    @Test
    public void getGamesByPlayerTest() {
        Player player = new Player();
        player.setId(1L);
        // mock

        // given
        List<Game> original = new ArrayList<Game>();
        original.add(new Game());
        original.add(new Game());

        when(gameRepository.findByFirstPlayerOrSecondPlayer(any(), any())).thenReturn(original);

        List<Game> accepted = new ArrayList<>();
        accepted.add(new Game());
        accepted.add(new Game());

        //when
        List<Game> actual = gameService.getGamesByPlayer(player);

        //then
        assertEquals(accepted.size(), actual.size());
    }

    @Test
    public void getAllGamesTest() {
        // mock - given
        List<Game> original = new ArrayList<Game>();
        original.add(new Game());
        original.add(new Game());

        when(gameRepository.findAll()).thenReturn(original);

        //when
        List<Game> actual = gameService.getAllGames();

        //then
        assertEquals(2, actual.size());
    }

    @Test
    public void getGameByIdTest() {
        // mock - given
        Game accepted = new Game();
        accepted.setGameStatus(GameStatus.WAITS_FOR_PLAYER);
        accepted.setId(2L);
        accepted.setGameType(GameType.COMPETITION);
        accepted.setSecondPlayer(null);
        accepted.setFirstPlayer(null);

        when(gameRepository.findById(anyLong())).thenReturn(Optional.of(accepted));

        //when
        Game actual = gameService.getGameById(1L);

        //then
        assertEquals(accepted.getId(), actual.getId());
        assertEquals(accepted.getGameType(), actual.getGameType());
        assertEquals(accepted.getGameStatus(), actual.getGameStatus());
        assertEquals(accepted.getSecondPlayer(), actual.getSecondPlayer());
        assertEquals(accepted.getFirstPlayer(), actual.getFirstPlayer());
    }


    @Test
    public void joinGamePlayersNotEqualsTest() {
        // mock - given

        Player player = new Player();

        Game original = new Game();
        original.setGameStatus(GameStatus.WAITS_FOR_PLAYER);
        original.setId(2L);
        original.setGameType(GameType.COMPETITION);
        original.setSecondPlayer(null);
        original.setFirstPlayer(null);
        original.setFirstPlayer(player);

        //when
        Game actual = gameService.joinGame(player, original);
        Game accepted = original;

        //then
        assertEquals(accepted.getId(), actual.getId());
        assertEquals(accepted.getGameType(), actual.getGameType());
        assertEquals(accepted.getGameStatus(), actual.getGameStatus());
        assertEquals(accepted.getSecondPlayer(), actual.getSecondPlayer());
        assertEquals(accepted.getFirstPlayer(), actual.getFirstPlayer());
    }

    @Test
    public void joinGamePlayersEqualsTest() {
        // given

        Player player = new Player();
        player.setUserName("ALEX");

        Game original = new Game();
        original.setGameStatus(GameStatus.WAITS_FOR_PLAYER);
        original.setId(2L);
        original.setGameType(GameType.COMPETITION);
        original.setSecondPlayer(null);
        original.setFirstPlayer(null);
        original.setFirstPlayer(new Player());

        Game accepted = new Game();
        accepted.setGameStatus(GameStatus.IN_PROGRESS);
        accepted.setId(2L);
        accepted.setGameType(GameType.COMPETITION);
        accepted.setSecondPlayer(null);
        accepted.setFirstPlayer(null);
        accepted.setFirstPlayer(new Player());

        // mock
        when(gameRepository.save(any())).thenReturn(accepted);

        //when
        Game actual = gameService.joinGame(player, original);

        //then
        assertEquals(accepted.getId(), actual.getId());
        assertEquals(accepted.getGameType(), actual.getGameType());
        assertEquals(accepted.getGameStatus(), actual.getGameStatus());
        assertEquals(accepted.getSecondPlayer(), actual.getSecondPlayer());
        assertEquals(accepted.getFirstPlayer(), actual.getFirstPlayer());
    }

    @Test
    public void checkCurrentGameStatusWaitingForPlayerTest() {
        // given

        Game game = new Game();
        game.setGameType(GameType.COMPETITION);
        game.setSecondPlayer(null);

        // mock
        when(gameLogicService.isWinner(any())).thenReturn(false);

        //when
        GameStatus actual = gameService.checkCurrentGameStatus(game, new Board());

        //then
        assertEquals(GameStatus.WAITS_FOR_PLAYER, actual);
    }

    @Test
    public void checkCurrentGameStatusFirstPlayerWonTest() {
        // given

        Board board = new Board();
        board.setNextPlayer(NextPlayer.FIRSTPLAYER);

        // mock
        when(gameLogicService.isWinner(any())).thenReturn(true);

        //when
        GameStatus actual = gameService.checkCurrentGameStatus(null, board);

        //then
        assertEquals(GameStatus.SECOND_PLAYER_WON, actual);
    }

    @Test
    public void checkCurrentGameStatusSecundPlayerWonTest() {
        // given

        Board board = new Board();
        board.setNextPlayer(NextPlayer.SECONDPLAYER);

        // mock
        when(gameLogicService.isWinner(any())).thenReturn(true);

        //when
        GameStatus actual = gameService.checkCurrentGameStatus(null, board);

        //then
        assertEquals(GameStatus.FIRST_PLAYER_WON, actual);
    }

    @Test
    public void checkCurrentGameStatusInProgressTest() {
        // given

        Game game = new Game();
        game.setGameType(GameType.COMPETITION);
        game.setSecondPlayer(new Player());

        // mock
        when(gameLogicService.isWinner(any())).thenReturn(false);

        //when
        GameStatus actual = gameService.checkCurrentGameStatus(game, new Board());

        //then
        assertEquals(GameStatus.IN_PROGRESS, actual);
    }

    @Test
    public void getGamesWaitingForSecondPlayerNoGamesFoundTest() {
        // given
        Player player = new Player();
        player.setId(2L);

        Game game = new Game();
        game.setFirstPlayer(player);

        List<Game> original = new ArrayList<Game>();
        original.add(game);

        // mock
        when(gameRepository.findByGameTypeAndGameStatus(GameType.COMPETITION, GameStatus.WAITS_FOR_PLAYER)).thenReturn(original);

        //when
        List<Game> actual = gameService.getGamesWaitingForSecondPlayer(player);

        //then
        assertEquals(0, actual.size());
    }

    @Test
    public void getGamesWaitingForSecondPlayerTest() {
        // given
        Player player = new Player();
        player.setId(2L);

        Game game = new Game();
        game.setFirstPlayer(player);

        List<Game> original = new ArrayList<Game>();
        original.add(game);

        // mock
        when(gameRepository.findByGameTypeAndGameStatus(GameType.COMPETITION, GameStatus.WAITS_FOR_PLAYER)).thenReturn(original);

        //when
        List<Game> actual = gameService.getGamesWaitingForSecondPlayer(new Player());

        //then
        assertEquals(1, actual.size());
    }


}
