package com.elte.malom.service;

import com.elte.malom.entities.Board;
import com.elte.malom.entities.Game;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.utils.BoardUtil;
import junitparams.JUnitParamsRunner;
import junitparams.NamedParameters;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.core.convert.support.ConfigurableConversionService;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(JUnitParamsRunner.class)
public class GameLogicServiceParameterisedTest {

    @NamedParameters("checkGameModeParameters")
    private Object[] checkGameModeParameters() {
        return new Object[]{
                new Object[]{new Board(new Game(), "[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date()), GameMode.OPENING},
                new Object[]{new Board(new Game(), "[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date()), GameMode.MIDGAME}
        };
    }

    @NamedParameters("checkGameModeFalseParameters")
    private Object[] checkGameModeFalseParameters() {
        return new Object[]{
                new Object[]{new Board(new Game(), "[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date()), GameMode.MIDGAME},
                new Object[]{new Board(new Game(), "[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date()), GameMode.OPENING}
        };
    }

//    @NamedParameters("testIsWinnerEndGameParameters")
//    private Object[] testIsWinnerEndGameParameters() {
//        return new Object[]{
//                new Object[]{true, new Integer[]{1,1,0,0,0,0,2,0,0,2,2,2}, "[1,1,0,0,0,0,2,0,0,2,2,2]"},
//                new Object[]{false, new Integer[]{1,1,1,0,0,0,2,0,0,2,2,2}, "[1,1,1,0,0,0,2,0,0,2,2,2]"}
//        };
//    }


    @Mock
    private BoardUtil boardUtil;
    @Mock
    private ConfigurableConversionService configurableConversionService;

    private GameLogicService gameLogicService;

    @Before
    public void init() {
        gameLogicService = new GameLogicService(configurableConversionService, boardUtil);
    }

//    @Test
//    @Parameters(method = "testIsWinnerEndGameParameters")
//    public void isWinnerEndGameTest(Boolean accepted, Integer[] array2, String boardString) {
//
//        // mock
////        List<Integer> list = Arrays.asList(array);
//        Integer[] array = {1,1,0,0,0,0,2,0,0,2,2,2};
//        List<Integer> list = Arrays.asList(array);
//        when(boardUtil.boardStringToArray(anyString())).thenReturn(list);
//
//        //given
//        Board board = new Board(new Game(), boardString, GameMode.ENDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
//
//        //when
//        Boolean actual = gameLogicService.isWinner(board);
//
//        //then
//        Assert.assertEquals(accepted, actual);
//
//    }

    @Test
    @Parameters(method = "checkGameModeParameters")
    public void checkGameModeTest(Board lastBoardStatus, GameMode accepted) {

        //when
        GameMode actual = gameLogicService.checkGameMode(lastBoardStatus);

        //then
        assertEquals(accepted, actual);
    }


    @Test
    @Parameters(method = "checkGameModeFalseParameters")
    public void checkGameModeNegativeTest(Board lastBoardStatus, GameMode accepted) {

        //when
        GameMode actual = gameLogicService.checkGameMode(lastBoardStatus);

        //then
        assertNotEquals(accepted, actual);
    }

}
