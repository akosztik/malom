package com.elte.malom.service;

import com.elte.malom.domain.Move;
import com.elte.malom.entities.Board;
import com.elte.malom.enums.GameMode;
import com.elte.malom.enums.MoveType;
import com.elte.malom.enums.NextPlayer;
import com.elte.malom.entities.Game;
import com.elte.malom.utils.BoardUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class GameLogicServiceTest {

    @Mock
    private BoardUtil boardUtil;
    @Mock
    private ConfigurableConversionService configurableConversionService;

    private GameLogicService gameLogicService;

    @Before
    public void init() {
        gameLogicService = new GameLogicService(configurableConversionService, boardUtil);
    }

    @Test
    public void isWinnerEndGameTrueTest() {
        // mock
        Integer[] array = {1, 1, 0, 0, 0, 0, 2, 0, 0, 2, 2, 2};
        List<Integer> list = Arrays.asList(array);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

        //given
        Board board = new Board(new Game(), "[1,1,0,0,0,0,2,0,0,2,2,2]", GameMode.ENDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Boolean accepted = true;

        //when
        Boolean actual = gameLogicService.isWinner(board);

        //then
        assertEquals(accepted, actual);
    }

    @Test
    public void isWinnerEndGameFalseTest() {
        // mock
        Integer[] array = {1, 1, 1, 0, 0, 0, 2, 0, 0, 2, 2, 2};
        List<Integer> list = Arrays.asList(array);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

        //given
        Board board = new Board(new Game(), "[1,1,1,0,0,0,2,0,0,2,2,2]", GameMode.ENDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Boolean accepted = false;

        //when
        Boolean actual = gameLogicService.isWinner(board);

        //then
        assertEquals(accepted, actual);
    }

    @Test
    public void isWinnerMidGameEnemyDegreeOfFreedomZeroTest() {
        // mock
        Integer[] array = {1, 2, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 1, 2, 1};
        List<Integer> list = Arrays.asList(array);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

        //given
        Board board = new Board(new Game(), "[1,2,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Boolean accepted = true;

        //when
        Boolean actual = gameLogicService.isWinner(board);

        //then
        assertEquals(accepted, actual);
    }

    @Test
    public void isWinnerMidGameEnemyDegreeOfFreedomNotZeroTest() {
        // mock
        Integer[] array = {1, 0, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 1, 2, 1};
        List<Integer> list = Arrays.asList(array);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(list);

        //given
        Board board = new Board(new Game(), "[1,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Boolean accepted = false;

        //when
        Boolean actual = gameLogicService.isWinner(board);

        //then
        assertEquals(accepted, actual);
    }

    @Test
    public void hitOnBoardFirstPlayerTest() {
        //mock

        Board board = new Board(new Game(), "[1,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 0, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 1, 2, 1));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]");

        //given
        Board lastBoardSattus = new Board(new Game(), "[1,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());
        Move move = new Move();
        move.setTo(0);
        move.setMoveType(MoveType.HIT);
        Board accepted = new Board(new Game(), "[0,0,1,0,0,0,0,0,0,2,2,0,0,2,2,0,0,0,0,2,0,1,2,1]", GameMode.MIDGAME, GameMode.OPENING, 0, 7, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.hitOnBoard(lastBoardSattus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        Assert.assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        Assert.assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void hitOnBoardFirstPlayerWithGoToEndGameTest() {
        //mock

        Board board = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,1]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 0, 2, 1));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1]");

        //given
        Board lastBoardSattus = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,1]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());
        Move move = new Move();
        move.setTo(22);
        move.setMoveType(MoveType.HIT);
        Board accepted = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1]", GameMode.MIDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.hitOnBoard(lastBoardSattus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        Assert.assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        Assert.assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void hitOnBoardSecondPlayerWithGoToEndGameTest() {
        //mock

        Board board = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,1]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.HIT, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 0, 2, 1));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,0]");

        //given
        Board lastBoardSattus = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,1]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.HIT, new Date());
        Move move = new Move();
        move.setTo(23);
        move.setMoveType(MoveType.HIT);
        Board accepted = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,2,0]", GameMode.ENDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.hitOnBoard(lastBoardSattus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        Assert.assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        Assert.assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void hitOnBoardFirstPlayerWithGoToEndGameTest_() {
        //mock

        Board board = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1]", GameMode.MIDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,0,0,1]");

        //given
        Board lastBoardSattus = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,0,0,0,1]", GameMode.MIDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setFrom(19);
        move.setTo(20);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,0,0,1]", GameMode.MIDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.hitOnBoard(lastBoardSattus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
        Assert.assertEquals(accepted.getGameModePlayer2(), actual.getGameModePlayer2());
        Assert.assertEquals(accepted.getGameModePlayer1(), actual.getGameModePlayer1());
        assertEquals(accepted.getPieceCountPlayer1(), actual.getPieceCountPlayer1());
        assertEquals(accepted.getPieceCountPlayer2(), actual.getPieceCountPlayer2());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeMoveNoMillCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(0);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 8, 9, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeHitMillTypeTwoDeepWithCornerPieceCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(2);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 6, 6, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeHitMillTypeOneDeepCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(1);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 6, 6, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeHitMillTypeOneDeepWithCentralPieceCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(4);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 6, 6, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithSecondPlayerAndNextMoveTypeHitMillTypeOneDeepCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2]", GameMode.OPENING, GameMode.OPENING, 6, 7, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2]", GameMode.OPENING, GameMode.OPENING, 6, 7, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(22);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 6, 6, NextPlayer.SECONDPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeHitMillTypeTwoDeepWithCentralPieceCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 7, 6, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(1);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,1,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2]", GameMode.OPENING, GameMode.OPENING, 6, 6, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithSecondPlayerAndNextMoveTypeMoveNoMillCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 9, 9, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(0);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]", GameMode.OPENING, GameMode.OPENING, 9, 8, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithSecondPlayerAndNextMoveTypeHitMillTypeTwoDeepGameModeSwitchToMidGameCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,2,2,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.MIDGAME, GameMode.OPENING, 0, 1, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,2,0,2,2,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,2,2,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.MIDGAME, GameMode.OPENING, 0, 1, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(1);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,2,0,2,2,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeMoveMillTypeTwoDeepGameModeSwitchToMidGameCreatedTest() {
        //mock
        Board board = new Board(new Game(), "[0,0,0,1,1,1,1,0,1,1,1,0,0,0,2,2,2,2,2,2,2,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,1,0,1,1,1,1,0,1,1,1,0,0,0,2,2,2,2,2,2,2,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,0,0,1,1,1,1,0,1,1,1,0,0,0,2,2,2,2,2,2,2,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(1);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,1,0,1,1,1,1,0,1,1,1,0,0,0,2,2,2,2,2,2,2,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeMoveNoMillCreatedNeighboursCountFourTest() {
        //mock
        Board board = new Board(new Game(), "[0,2,2,2,0,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,2,2,2,1,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,2,2,2,0,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(4);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,2,2,2,1,2,2,2,2,2,2,0,0,0,1,1,1,1,1,1,1,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeMoveNoMillCreatedNeighboursCountTwoTest() {
        //mock
        Board board = new Board(new Game(), "[0,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 1, 0, 2, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 1, 1, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[1,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]", GameMode.OPENING, GameMode.MIDGAME, 1, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setTo(0);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[1,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeMoveGameModeMidGameTest() {
        //mock
        Board board = new Board(new Game(), "[0,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 1, 0, 2, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0, 1, 1, 1, 2, 2, 1, 1, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,1,0,2,0,2,2,2,2,0,0,0,0,1,1,1,1,2,2,1,0,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,1,0,2,0,2,2,2,2,0,0,0,0,0,1,1,1,2,2,1,1,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setFrom(20);
        move.setTo(13);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,1,0,2,0,2,2,2,2,0,0,0,0,1,1,1,1,2,2,1,0,0,0,0]", GameMode.MIDGAME, GameMode.MIDGAME, 0, 0, NextPlayer.SECONDPLAYER, MoveType.MOVE, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }

    @Test
    public void moveOnBoardwithFirstPlayerAndNextMoveTypeHitGameModeEndGameTest() {
        //mock
        Board board = new Board(new Game(), "[0,1,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,2,0,1,1,0,0,0]", GameMode.ENDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());

        when(configurableConversionService.convert(any(), eq(Board.class))).thenReturn(board);
        when(boardUtil.boardStringToArray(anyString())).thenReturn(Arrays.asList(0, 1, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0));
        when(boardUtil.boardArrayToString(anyList())).thenReturn("[0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,2,1,1,1,0,0,0]");

        //given
        Board lastBoardStatus = new Board(new Game(), "[0,1,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,2,0,1,1,0,0,0]", GameMode.ENDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.MOVE, new Date());
        Move move = new Move();
        move.setFrom(1);
        move.setTo(18);
        move.setMoveType(MoveType.MOVE);
        Board accepted = new Board(new Game(), "[0,0,0,2,0,2,0,0,0,0,0,0,0,0,0,0,0,2,1,1,1,0,0,0]", GameMode.ENDGAME, GameMode.ENDGAME, 0, 0, NextPlayer.FIRSTPLAYER, MoveType.HIT, new Date());

        //when
        Board actual = gameLogicService.moveOnBoard(lastBoardStatus, move);

        //then
        assertEquals(accepted.getBoard(), actual.getBoard());
        Assert.assertEquals(accepted.getNextMoveType(), actual.getNextMoveType());
        Assert.assertEquals(accepted.getNextPlayer(), actual.getNextPlayer());
    }
}
